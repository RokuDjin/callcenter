package com.jdc.callcenter;

import java.util.Random;

/**
 * A Discrete Random Number Generator
 *
 */

public class DiceRoller {
	
	private static final Random RNG = new Random();
	
	public static int rollDice() 
	{
		return RNG.nextInt(6)+1;
	}
	
	public static int rollFourSidedDice() 
	{
		return RNG.nextInt(4)+1;
	}

}
