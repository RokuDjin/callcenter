package com.jdc.callcenter;

public class ProductManager extends Employee {

	public ProductManager(String name) 
	{
		super(name);
	}

	@Override
	public void solveCall(Call call) throws InterruptedException 
	{
		/* Pass time to simulate an actual call session with length */
		call.accumulateTime();
		
		endCall(call);
	}
	

}
