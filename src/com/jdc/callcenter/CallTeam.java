package com.jdc.callcenter;

/**
 * Enumeration of the Call Team positions. 
 * Add new positions or adjust the number of each employee here.
 * 
 */

public enum CallTeam {
	
	FRESHER(0,5),				// Level 0, 5 FRESHERS
	TECHNICAL_LEAD(1,1),		// Level 1, 1 TECHNICAL LEAD
	PRODUCT_MANAGER(2,1);		// Level 2, 1 PRODUCT MANAGER
	
	private static int levels = CallTeam.values().length;
	
	private int employeeLevel;
	private int numEmployees;
	
	private CallTeam(int level, int numEmployees) 
	{
		this.employeeLevel = level;
		this.numEmployees = numEmployees;
	}
	
	public static int getLevels() 
	{
		return levels;
	}
	
	public int getEmployeeLevel() 
	{
		return employeeLevel;
	}
	
	public int getNumberOfEmployees() 
	{
		return numEmployees;
	}
	
}
