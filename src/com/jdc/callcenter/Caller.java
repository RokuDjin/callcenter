package com.jdc.callcenter;

/**
 * A Caller is a 'process' that creates a telephone call by sending a request
 * to the instance CallRequestHandler
 * 
 */

public class Caller implements Runnable {
	
	private CallRequestHandler callRequestHandler;
	private String name;
	
	public Caller(String name) throws InvalidTeamException 
	{
		this.callRequestHandler = CallRequestHandler.getInstance();
		this.name = name;
	}
	
	public String getName() 
	{
		return name;
	}
	
	@Override
	public void run() {
		callRequestHandler.requestCall(this);
	}

}
