package com.jdc.callcenter;

/**
 * A simple, busy-waiting lock.
 *
 */

public class Lock {
	
	private boolean busy;
	
	public Lock() 
	{
		this.busy = false;
	}
	
	public synchronized void lock() throws InterruptedException 
	{
		while (busy)	wait();
		busy = true;
	}
	
	public synchronized void unlock() 
	{
		busy = false;
		notify();
	}
	
}
