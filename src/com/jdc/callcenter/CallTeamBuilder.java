package com.jdc.callcenter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * A Builder Class for initializing the worker threads in the CallRequestHandler class. 
 * Based on the CallTeam enumerations defined
 */

public class CallTeamBuilder {	
	
	/** 
	 * Create and return lists of CallReceivers for each queue in requestQueues
	 * 
	 */
	public List<List<CallReceiver>> assembleCallTeam(List<BlockingQueue<Call>> requestQueues) throws InvalidTeamException 
	{
		/* Initialize Lists of CallReceivers to return */
		int totalLevels = CallTeam.getLevels();
		List<List<CallReceiver>> requestReceivers = new ArrayList<List<CallReceiver>>(totalLevels);
		
		CallTeam[] team = CallTeam.values();
		
		/* Create List of CallReceivers */
		for (CallTeam position : team) 
			requestReceivers.add(createListOfReceivers(position, requestQueues));

		return requestReceivers;
	}
	
	/**
	 * Create and return a list of CallReceivers of a given CallTeam position
	 *
	 */
	private List<CallReceiver> createListOfReceivers(CallTeam position, List<BlockingQueue<Call>> requestQueues) throws InvalidTeamException  
	{
		/* Initialize List of CallReceivers to return */
		int numEmployees = position.getNumberOfEmployees();
		List<CallReceiver> listOfCallReceivers = new ArrayList<CallReceiver>(numEmployees);
		
		/* Get corresponding Queue based on position level */
		int positionLevel = position.getEmployeeLevel();
		BlockingQueue<Call> requestQueue = requestQueues.get(positionLevel);
		
		Employee employee;
		CallReceiver callReceiver;
		
		/* Create CallReceivers*/
		for (int i=0; i<numEmployees; i++) 
		{
			employee = createEmployee(position, i);
			callReceiver = new CallReceiver(employee, requestQueue);
			listOfCallReceivers.add(callReceiver);
		}
		
		return listOfCallReceivers;
	}
	
	/**
	 * Create an Employee based on Call Team position
	 * 
	 */
	private Employee createEmployee(CallTeam position, int id) throws InvalidTeamException  
	{
		if (position == CallTeam.FRESHER)
			return new Fresher(position.toString() + "_" + Integer.toString(id+1));
		if (position == CallTeam.TECHNICAL_LEAD)
			return new TechnicalLead(position.toString() + "_" + Integer.toString(id+1));
		if (position == CallTeam.PRODUCT_MANAGER)
			return new ProductManager(position.toString() + "_" + Integer.toString(id+1));
		else
			throw new InvalidTeamException("Case: " + position.toString() + " could not be found");
	}

}
