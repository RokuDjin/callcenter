package com.jdc.callcenter;

import java.util.Date;

/**
 * General purpose time-stamp used by a Call instance.
 *
 */

public class TimeStamp {
	
	private Date date;
	private int duration;
	
	public TimeStamp() 
	{
		this.date = new Date();
		this.duration = 0;
	}
	
	public Date getDate() 
	{
		return date;
	}
	
	public int getDuration() 
	{
		return duration;
	}
	
	public void extendDuration(int duration) 
	{
		this.duration += duration;
	}

}
