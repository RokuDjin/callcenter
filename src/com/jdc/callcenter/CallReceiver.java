package com.jdc.callcenter;

import java.util.concurrent.BlockingQueue;

/**
 * CallReceiver is a Pool Thread worker that receives a Call from a 
 * request queue and delegates it to the Employee it was assigned to.
 *
 */

public class CallReceiver extends Thread {
	
	/* A Blocking Queue the CallReceiver dequeues Calls from */
	private BlockingQueue<Call> callRequestQueue;
	
	/* An Employee that CallReceiver is assigned to */
	private Employee employee;

	public CallReceiver(Employee employee, BlockingQueue<Call> callRequestQueue) {
		this.employee = employee;
		this.callRequestQueue = callRequestQueue;
	}
	
	public void run() {
		while (true) {
			try {
				/* Get a hold of the Employee when they become free */
				Lock lock = employee.getLock();
				lock.lock();
				
				/* Delegate call to Employee */
				Call call = callRequestQueue.take();
				call.assignTo(employee);
				
				/* Set the Employee free */
				lock.unlock();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (InvalidTeamException e) {
				e.printStackTrace();
			}
		}
	}

}
