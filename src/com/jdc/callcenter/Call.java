package com.jdc.callcenter;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * The Call class. It models a Call Session between a Caller and and Employee
 *
 */

public class Call {
	
	/* Call id Counter */
	private static AtomicInteger idCount = new AtomicInteger(0);
	private int callID;
	
	/* Call Relationship */
	private Caller caller;
	private Employee employee;
	
	private int level;
	private TimeStamp timeStamp;
	private String callLog;
	
	public Call(Caller caller) 
	{
		this.callID = idCount.incrementAndGet();
		this.caller = caller;
		this.level = 0;
		this.timeStamp = new TimeStamp();
		this.callLog = "Call Session #" + callID + " created for " + caller.getName() + " on " + timeStamp.getDate() + ".\n";
	}
	
	public Caller getCaller() 
	{
		return caller;
	}
	
	public Employee getCallHandler() 
	{
		return employee;
	}
	
	public int getLevel() 
	{
		return level;
	}
	
	public void raiseLevel() 
	{
		level++;
	}
	
	public void sendMessage(String message) 
	{
		callLog += message + "\n";
	}
	
	public void assignTo(Employee employee) throws InterruptedException, InvalidTeamException 
	{			
		/* Assign and handle call with Employee */
		this.employee = employee;
		employee.receiveCall(this);
	}
	
	public void end() 
	{
		sendMessage("Call Duration: " + timeStamp.getDuration() + " seconds.");
		System.out.println(callLog);
	}
	
	public void accumulateTime() throws InterruptedException
	{
		/* Update TimeStamp with a randomly generated call time */
		int callDuration = DiceRoller.rollFourSidedDice();
		timeStamp.extendDuration(callDuration);
		
		/* Wait for (callDuration) seconds to simulate a call session with length */
		Thread.sleep(callDuration * 1000);
	}
	
}
