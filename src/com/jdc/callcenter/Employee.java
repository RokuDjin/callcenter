package com.jdc.callcenter;

/**
 * An Employee class that, at the moment, only handles Call requests.
 *
 */

public class Employee {
	
	private String name;
	private Lock lock;
	
	public Employee(String name) 
	{
		this.name = name;
		this.lock = new Lock();
	}
	
	public String getName() 
	{
		return name;
	}
	
	public Lock getLock() 
	{
		return lock;
	}
	
	public void receiveCall(Call call) throws InterruptedException, InvalidTeamException
	{
		call.sendMessage(getName() + " has received the call.");
		solveCall(call);
	}
	
	public void solveCall(Call call) throws InterruptedException, InvalidTeamException 
	{
		call.sendMessage("Attempting to solve issue...");
		
		if (DiceRoller.rollDice() > 1) 		endCall(call);
		else 								escalateCall(call);
	}
	
	public void endCall(Call call) throws InterruptedException 
	{
		call.sendMessage("Issue solved by " + getName() + ".");
		
		/* Pass time to simulate an actual call session with length */
		call.accumulateTime();
		
		call.end();
	}
	
	public void escalateCall(Call call) throws InterruptedException, InvalidTeamException 
	{
		call.sendMessage(
				"Call could not be solved by " 
				+ getName() 
				+ ". This call will be escalated to a higher level."
			);
		
		/* Pass time to simulate an actual call session with length */
		call.accumulateTime();
			
		/* Dispatch Call back to the CallRequestHandler instance at the next level */
		CallRequestHandler callRequestHandler = CallRequestHandler.getInstance();
		callRequestHandler.escalateCall(call);
	}

}
