package com.jdc.callcenter;

/** 
 * CallRequestHandler is a Thread-Pool with CallReceiver 
 * worker threads handling Call requests from a designated Queue
 * 	
 */

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class CallRequestHandler {
	
	private static CallRequestHandler instance;
	
	/* Call Queues indexed by the Call's level */
	private List<BlockingQueue<Call>> requestQueues;
	
	/* List of CallReceiver workers */
	private List<List<CallReceiver>> requestReceivers;

	private CallRequestHandler() throws InvalidTeamException 
	{
		/* Initialize Call Request Queues */
		requestQueues = new ArrayList<BlockingQueue<Call>>();
		int totalLevels = CallTeam.getLevels();
		for (int i=0; i<totalLevels; i++)	
			requestQueues.add(new LinkedBlockingQueue<Call>());
		
		/* Initialize lists of CallReceivers for each Call Request Queue */
		CallTeamBuilder teamBuilder = new CallTeamBuilder();
		requestReceivers = teamBuilder.assembleCallTeam(requestQueues);
		
		/* Start Each CallReceiver thread */
		for (List<CallReceiver> listOfCallReceivers : requestReceivers) 
			for (CallReceiver callReceiver : listOfCallReceivers)
				callReceiver.start();
	}
	
	/* Singleton Instance getter */
	public static CallRequestHandler getInstance() throws InvalidTeamException
	{
		if (instance == null)		instance = new CallRequestHandler();
		return instance;
	}
	
	public void requestCall(Caller caller) 
	{
		Call call = new Call(caller);
		enqueueCallRequest(call);
	}
	
	public void escalateCall(Call call) 
	{
		call.raiseLevel();
		enqueueCallRequest(call);
	}
	
	private void enqueueCallRequest(Call call) 
	{
		try {
			int level = call.getLevel();
			requestQueues.get(level).put(call);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
