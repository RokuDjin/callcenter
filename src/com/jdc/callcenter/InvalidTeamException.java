package com.jdc.callcenter;

/**
 * The InvalidTeamException class. This exception is thrown by the createEmployee method
 * in the CallTeamBuilder class when it is unable to create and return an Employee 
 *
 */

public class InvalidTeamException extends Exception {

	/**
	 * 	Default Serial Version ID
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTeamException() 
	{
		super();
	}
	
	public InvalidTeamException(String message) 
	{
		super(message);
	}
}
