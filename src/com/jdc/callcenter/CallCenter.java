package com.jdc.callcenter;

/**
 * The CallCenter class. This class is used as a starting point to simulate impromptu
 * telephone calls to a call center team.
 * 
 * @author Juliane dela Cruz
 * 
 */

public class CallCenter {
	
	public static void main(String[] args) 
	{
		try {
			/* Initialize the Call Center Team before making any requests */
			CallRequestHandler.getInstance();
			
			Thread callerThread = new Thread(new Caller("Fly"));
			Thread t1 = new Thread(new Caller("Polar"));
			Thread t2 = new Thread(new Caller("DOG"));
			Thread t3 = new Thread(new Caller("ADSF"));
			Thread t4 = new Thread(new Caller("QWERTY"));
			Thread t5 = new Thread(new Caller("DERP"));
			Thread t6 = new Thread(new Caller("HERP"));
			
			callerThread.run();t1.run(); t2.run(); t3.run(); t4.run(); t5.run(); t6.run();
			
			callerThread.join(); t1.join(); t2.join(); t3.join(); t4.join(); t5.join(); t6.join();
		} catch (InvalidTeamException e) {
			System.out.println("Error in Processing Call Center Thread Pool");
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("A Thread has been interrupted");
			e.printStackTrace();
		}
	}

}
